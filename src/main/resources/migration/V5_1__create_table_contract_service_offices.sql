CREATE TABLE contract_service_offices
(
    id          int PRIMARY KEY AUTO_INCREMENT,
    contract_id int,
    office_id   int,
    FOREIGN KEY (contract_id) REFERENCES contracts (id),
    FOREIGN KEY (office_id) REFERENCES offices (id)
)
