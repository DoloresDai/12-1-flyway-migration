CREATE TABLE staff
(
    id         int PRIMARY KEY AUTO_INCREMENT,
    first_name varchar(30),
    last_name  varchar(30),
    office_id  int,
    FOREIGN KEY (office_id) REFERENCES offices (id)
)
