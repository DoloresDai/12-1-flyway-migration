CREATE TABLE clients(
    id int PRIMARY KEY AUTO_INCREMENT,
    full_name varchar(128),
    abbreviation varchar(6)
)
