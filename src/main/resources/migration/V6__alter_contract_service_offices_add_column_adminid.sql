ALTER TABLE contract_service_offices
    ADD admin_id int;
ALTER TABLE contract_service_offices
    ADD FOREIGN KEY (admin_id) REFERENCES staff (id);
