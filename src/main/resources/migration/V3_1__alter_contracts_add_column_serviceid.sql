ALTER TABLE contracts
    ADD service_id int;
ALTER TABLE contracts
    ADD FOREIGN KEY (service_id) REFERENCES services (id);
