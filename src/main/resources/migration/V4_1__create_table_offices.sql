CREATE TABLE offices
(
    id      int PRIMARY KEY AUTO_INCREMENT,
    country varchar(50),
    city    varchar(50)
)
