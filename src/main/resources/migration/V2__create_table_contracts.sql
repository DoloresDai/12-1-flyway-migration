CREATE TABLE contracts
(
    id        int PRIMARY KEY AUTO_INCREMENT,
    name      varchar(128),
    client_id int,
    FOREIGN KEY (client_id) REFERENCES clients(id)
)
